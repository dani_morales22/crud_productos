﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaLogicaNegocio;

namespace CapaPresentacion
{
    public partial class Form1 : Form
    {      
        public Form1()
        {
            InitializeComponent();
        }

        CN_Productos objproducto = new CN_Productos();
        string Operacion = "Insertar";
        string idprod;

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (Operacion == "Insertar")
            {
                objproducto._IdCategoria = Convert.ToInt32(CmbCategoria.SelectedValue);
                objproducto._IdMarca = Convert.ToInt32(CmbMarca.SelectedValue);
                objproducto._Descripcion = txtDescripcion.Text;
                objproducto._Precio = Convert.ToDouble(txtPrecio.Text);
                objproducto.InsertarProductos();

                MessageBox.Show("Se inserto correctamente");
            }
            else if (Operacion == "Editar")
            {
                objproducto._IdCategoria = Convert.ToInt32(CmbCategoria.SelectedValue);
                objproducto._IdMarca = Convert.ToInt32(CmbMarca.SelectedValue);
                objproducto._Descripcion = txtDescripcion.Text;
                objproducto._Precio = Convert.ToDouble(txtPrecio.Text);
                objproducto._Idprod = Convert.ToInt32(idprod);

                objproducto.EditarProductos();
                Operacion = "Insertar";
                MessageBox.Show("Se edito correctamente");
            }
            ListarProductos();
            LimpiarFormulario();
        }

        private void ListarProductos()
        { 
            dataGridView1.DataSource = objproducto.ListarProductos();
        }

        private void ListarCategorias()
        {
            CmbCategoria.DataSource = objproducto.ListarCategorias();
            CmbCategoria.DisplayMember = "CATEGORIA";
            CmbCategoria.ValueMember = "IDCATEG";
        }
        private void ListarMarcas()
        {
            CmbMarca.DataSource = objproducto.ListarMarcas();
            CmbMarca.DisplayMember = "MARCA";
            CmbMarca.ValueMember = "IDMARCA";
        }

        private void LimpiarFormulario()
        {
            txtDescripcion.Clear();
            txtPrecio.Clear();
        }

        private void CmbCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Operacion = "Editar";
                CmbCategoria.Text = dataGridView1.CurrentRow.Cells["CATEGORIA"].Value.ToString();
                CmbMarca.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                txtDescripcion.Text = dataGridView1.CurrentRow.Cells["DESCRIPCION"].Value.ToString();
                txtPrecio.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                idprod = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
            }
            else
                MessageBox.Show("debe seleccionar una fila");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                objproducto._Idprod = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                objproducto.EliminarProducto();
                MessageBox.Show("Se elimino satisfactoriamente");
                ListarProductos();
            }
            else
            {
                MessageBox.Show("Seleccione una fila");
            }
            LimpiarFormulario();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ListarCategorias();
            ListarMarcas();
            ListarProductos();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
